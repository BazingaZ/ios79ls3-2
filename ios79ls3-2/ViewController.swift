//
//  ViewController.swift
//  ios79ls3-2
//
//  Created by WA on 05.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print(calculate(x: 10, y: 20))
    }

    func calculate(x: Int, y: Int) -> Int {
        return x + y
    }

}

